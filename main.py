#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from os import environ
import re
import random

from uuid import uuid4

from telegram import (
    InlineQueryResultArticle,
    ParseMode,
    InputTextMessageContent,
)
from telegram.ext import Updater, InlineQueryHandler, CommandHandler
from telegram.utils.helpers import escape_markdown

"""
First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Basic inline bot example. Applies different text transformations.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    level=logging.INFO,
)

logger = logging.getLogger(__name__)

die_str_re = re.compile(r"((?:[+\-]?|[+\-])\d)?d([1-2]?\d)|([+\-]\d)")

# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text("Hi, I'm your Alternity helper-bot. Currently all I can do is positive dice rolls, but more features are coming")


def help(update, context):
    """Send a message when the command /help is issued."""
    update.message.reply_text("Help? What's that?")

def create(update, context):
    """Send a message when the command /create is issued."""
    update.message.reply_text("The character creation function is currently disabled")

def str_to_int(s):
    return int(s) if s else 0

if message.chat.type == "private":
	# private chat message

if message.chat.type == "group":
	# group chat message

def roll_dice(dice_str):
    """List of all possible dice combinations"""
    situation_dice = [
        (1, -20),
        (1, -12),
        (1, -10),
        (1, -8),
        (1, -6),
        (1, -4),
        (0, 0),
        (1, 4)
        (1, 6),
        (1, 8),
        (1,10),
        (1, 12),
        (1, 20),
        (2, 20),
        (3, 20),
    ]

    dice_list = []
    

    for match in die_str_re.finditer(dice_str):
        dice_amount, die_type, situation_die = match.group(1, 2, 3)

        if situation_die:
            dice_list.append(situation_dice[int(situation_die) + 5])
        else:
            dice_list.append(
                (
                    int(dice_amount) if dice_amount is not None else 1,
                    # Concatenating those two in an effort to handle signs
                    int(die_type),
                )
            )

    results_list = []

    for die_tuple in dice_list:
        # correct handling of negative throws is hard
        for i in range(abs(die_tuple[0])):
            results_list.append(
                random.randint(1, die_tuple[1])
                * (-1 if die_tuple[0] < 0 else 1)
            )

    return f"Rolling {dice_str}… {' + '.join([str(e) for e in results_list])} = {sum(results_list)}"


def inlinequery(update, context):
    """Handle the inline query."""
    query = update.inline_query.query
    results = [
        InlineQueryResultArticle(
            id=uuid4(),
            title="Roll!",
            input_message_content=InputTextMessageContent(roll_dice(query)),
        )
    ]

    update.inline_query.answer(results)


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(environ["TG_TOKEN"], use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))

    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(InlineQueryHandler(inlinequery))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Block until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == "__main__":
    main()
